import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import com.google.gson.Gson;
/**
 * Created by sarvesh on 8/10/15.
 */
public class RepOkProxy implements RepOkInterface{
    private String urlToConnect = null;
    RepOkProxy (String urlToConnect)
    {
        this.urlToConnect = urlToConnect;
    }
    public boolean repOk ()
    {
        URL url = null;
        URLConnection connection = null;
        try {
            url = new URL(urlToConnect);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        try {
            connection = url.openConnection();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        connection.setDoOutput(true);
        try {
            OutputStreamWriter out = new OutputStreamWriter(
                    connection.getOutputStream());
            out.write ("helloworld");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader in = new BufferedReader(
                                new InputStreamReader(connection.getInputStream())
                                );
            String output = null;
            while ((output = in.readLine()) != null) {
                RepOkResponse response = new Gson().fromJson(output, RepOkResponse.class);
                in.close();
                if (response.result.equalsIgnoreCase("OK"))
                    return true;
                else
                    return false;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    class RepOkResponse
    {
        String result = null;
    }

    public static void main (String [] args)
    {
        RepOkProxy r = new RepOkProxy("http://webappvandv.appspot.com/repok");
        boolean result = r.repOk();
        System.out.println ("Result is " + result);
    }
}

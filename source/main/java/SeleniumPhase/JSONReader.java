//package SeleniumPhase.SeleniumPhase;

import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSONReader {
    FileReader reader;
    String fileName;
    String filePath;

    public String readFile(String path, String fileName){
        List<String> lines = new ArrayList<String>();
        Path pathToFile = Paths.get(path + fileName);
        try {
            lines = Files.readAllLines(pathToFile, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder s = new StringBuilder();
        for (String line : lines) {
            s.append(line);
        }
        //System.out.println(s.toString());
        return s.toString();
    }
    public String readFile() {
        return readFile(filePath, fileName);
    }

    public JSONReader(String fileName, String filePath, boolean startFromBaseFolder) {
        this.fileName = fileName;
        if (startFromBaseFolder) {
            this.filePath = System.getProperty("user.dir") + filePath;
        } else {
            this.filePath = filePath;
        }
    }

    public static void main(String[] args) {
        // Read a test file and output its contents to System.out
        String fileName = "testFile.json";
        String baseFolder = System.getProperty("user.dir");
        String path = baseFolder + "/Source/main/resources/";
        JSONReader reader = new JSONReader(fileName, path, false);
        //System.out.println(reader.readFile(path, fileName));
        String fileContents = reader.readFile(path, fileName);
        //System.out.println(fileContents);
        System.out.println(reader.parseFile(fileContents));
    }

    private String parseFile(String fileContents){
        Map<String, Object> mapObject = new Gson().fromJson(fileContents, Map.class);
        //System.out.println(mapObject.get("test_case1"));
        System.out.println(mapObject);
        //Object field0 = ((Map)mapObject.get("test_case1")).get("field_id0");
        //System.out.println(mapObject.get("test_case1"));

        // Top level - individual test cases
        for (String key : mapObject.keySet()) {
            System.out.println(key);
            Map<String, Object> fieldValues = (Map)mapObject.get(key);
            // Field names
            for (String fieldName : fieldValues.keySet()) {
                //System.out.println(fieldName + ": " + fieldValues.get(fieldName));
                Map<String, String> fieldAttributes = (Map)fieldValues.get(fieldName);
                // Key / Value pairs related to the field names
                // In this case, keys are Type and Value
                for (String attributeName : fieldAttributes.keySet()) {
                    String attributeValue = fieldAttributes.get(attributeName);
                    System.out.println(fieldName + " - " + attributeName + ": " + attributeValue);
                }
            }
        }
        return "end";
    }
}


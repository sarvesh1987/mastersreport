//package SeleniumPhase.SeleniumPhase;


import com.google.gson.Gson;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SeleniumTestCase {
    static class FormField {
        String fieldName;
        String fieldType;
        String fieldValue;
        public FormField(String fieldName, String fieldType, String fieldValue){
            this.fieldName = fieldName;
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
        }
    }
    List<FormField> fields;
    String fileContents;
    String url;

    public SeleniumTestCase(String fileContents) {
        // Read in information
        // Translate into a List of FormFields with their values
        /*fields = new ArrayList<FormField>();
        System.out.println(fileContents);
        Map<String, List<FormField>> testCases = parseFileContents(fileContents);
        System.out.println(testCases);*/
        this.fileContents = fileContents;
    }

    /**
     *
     * @param fileContents The String representation of a JSON file containing
     *                     field settings for a new test case
     * @return A Map, relating a String (test case name) to a list of FormFields
     *         containing the field name, type, and value for Selenium to fill in
     */
    private Map<String, List<FormField>> parseFileContents(String fileContents) {
        Map<String, List<FormField>> testCases = new HashMap<String, List<FormField>>();
        Map<String, Object> mapObject = new Gson().fromJson(fileContents, Map.class);
        //System.out.println(mapObject);

        // Top level - individual test cases
        for (String testCaseName : mapObject.keySet()) {
            List<FormField> fieldList = new ArrayList<FormField>();
            //System.out.println(testCaseName);
            Map<String, Object> fieldValues = (Map)mapObject.get(testCaseName);
            // Field names
            for (String fieldName : fieldValues.keySet()) {
                String fieldType;
                String fieldValue;
                //System.out.println(fieldName + ": " + fieldValues.get(fieldName));
                Map<String, String> fieldAttributes = (Map)fieldValues.get(fieldName);
                // Key / Value pairs related to the field names
                // In this case, keys are Type and Value
                fieldType = fieldAttributes.get("type");
                fieldValue = fieldAttributes.get("value");
                fieldList.add(new FormField(fieldName, fieldType, fieldValue));
            }
            testCases.put(testCaseName, fieldList);
        }
        return testCases;
    }

    public static void main(String[] args) {
        String url = "http://webappvandv.appspot.com/";
        //String jsonFileName;

        //WebDriver driver = new HtmlUnitDriver();

        //String fileName = "form2.html";
        //String baseFolder = System.getProperty("user.dir").replace("\\", "/");
        //String path = baseFolder + "/Source/main/resources/";
        //System.out.println(path + fileName);
        // And now use this to visit Google
        // driver.get("file://" + path + fileName);


        //driver.quit();
        //System.out.println();
        //System.out.println("--------------------");
        //System.out.println();


        JSONReader reader = new JSONReader("output.txt","/",true);
        SeleniumTestCase s = new SeleniumTestCase(reader.readFile());
        Map<String, List<FormField>> testCases = s.parseFileContents(s.fileContents);

        for (String caseName : testCases.keySet()) {
            System.out.println(caseName);
            for (FormField f : testCases.get(caseName)) {
                System.out.println(f.fieldName + " - " + f.fieldType + ": " + f.fieldValue);
            }
            s.completeForm(testCases.get(caseName), url);
        }
    }

    /**
     * Fill in the form based on the values in the fields argument
     * @param fields
     * @param url
     */
    private String completeForm(List<FormField> fields, String url) {
        // TODO: add a way to select different drivers
        //WebDriver driver = new HtmlUnitDriver();
        //driver = new ChromeDriver();
        //driver = new SafariDriver();
        WebDriver driver = new FirefoxDriver();
        //WebDriver driver = new ChromeDriver();
        driver.get(url);

        WebElement element = null;
        WebElement submitElement;// = driver.findElement(By.name("submit"));

        for (FormField field : fields) {
            element = driver.findElement(By.name(field.fieldName));
            // BEFORE:
            //System.out.println("Before: " + element);
            switch (field.fieldType) {
                case "checkbox":
                    if (field.fieldValue.equals("true")) {
                        if (!element.isSelected()) {
                            element.click();
                        }
                    } else if (field.fieldValue.equals("false")) {
                        if (element.isSelected()) {
                            element.click();
                        }
                    }
                    break;
                case "dropdown":
                    new Select(element).selectByValue(field.fieldValue);
                    break;
                case "text":
                    element.sendKeys(field.fieldValue);
                case "submit":
                    // We always want to handle the submit button last, but
                    // the order in a map is unknown
                    break;
            }
            //System.out.println("After: " + element);
        }

        // Wait prior to submitting for visual aid

        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        if (element != null) {
            element.submit();
        }
        // TODO: get the resulting page
        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.findElement(By.tagName("body")).getText());
        String returnUrl = driver.getCurrentUrl();
	    driver.quit();
        return returnUrl;
    }

    public void runTestCases(String url) {

        // First go through and get all the lines in the urls.txt file
        // as these will be the urls we will run tests on
        ArrayList<String> urlsToTest = new ArrayList <String> ();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("urls.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String urlToTest = null;
        while (true) {
            try {
                urlToTest = br.readLine();
                if (urlToTest == null)
                    break;
                else
                    urlsToTest.add (urlToTest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("urls.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Map<String, List<FormField>> testCases = parseFileContents(fileContents);
        // Apply the test cases to all URLs
        for (String s : urlsToTest) {
            for (String caseName : testCases.keySet()) {
                System.out.println(caseName);
                for (FormField f : testCases.get(caseName)) {
                    System.out.println(f.fieldName + " - " + f.fieldType + ": " + f.fieldValue);
                }
                String resultUrl = completeForm(testCases.get(caseName), s);
                writer.println(resultUrl);
            }
        }
        writer.close();
    }
}

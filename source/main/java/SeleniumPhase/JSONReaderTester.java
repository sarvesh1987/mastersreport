

import SeleniumPhase.SeleniumPhase.JSONReader;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Doug on 4/19/2015.
 */
public class JSONReaderTester {
    @Test
     public void jsonReaderTest1() {
        String fileName = "testFile.json";
        String filePath = "/test/resources/";
        boolean startFromBaseFolder = true;

        JSONReader reader = new JSONReader(fileName,filePath, startFromBaseFolder);
        String correctOutput = "{  \"test_case1\": {    \"field_id0\": {      \"type\": \"checkbox\",      \"value\": \"true\"    },    \"field_id1\": {      \"type\": \"dropdown\",      \"value\": \"a\"    },    \"field_id2\": {      \"type\": \"submit\",      \"value\": \"submit\"    }  }}";
        Assert.assertEquals(correctOutput, reader.readFile());
    }

    @Test
    public void jsonReaderTest2() {
        String fileName = "testFile2.json";
        String filePath = "/test/resources/";
        boolean startFromBaseFolder = true;

        JSONReader reader = new JSONReader(fileName,filePath, startFromBaseFolder);
        String correctOutput = "{  \"test_case1\": {    \"field_id0\": {      \"type\": \"checkbox\",      \"value\": \"true\"    },    \"field_id1\": {      \"type\": \"dropdown\",      \"value\": \"a\"    },    \"field_id2\": {      \"type\": \"submit\",      \"value\": \"submit\"    }  },  \"test_case2\": {    \"field_id3\": {      \"type\": \"checkbox\",      \"value\": \"false\"    },    \"field_id4\": {      \"type\": \"dropdown\",      \"value\": \"b\"    },    \"field_id5\": {      \"type\": \"submit\",      \"value\": \"submit\"    }  }}";
        Assert.assertEquals(correctOutput, reader.readFile());
    }
}

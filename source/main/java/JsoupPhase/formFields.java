//package JsoupPhase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class formFields {
	
	public static void main (String[] args) {
		jsoupParse test = new jsoupParse();
		formFields form = new formFields();
		Document doc = test.parseHTML("http://webappvandv.appspot.com/");
		form.findFields(doc);
	}
	
	/*
	 * Extract check boxes, drop downs (and their values), 
	 * submit button, and buttons in general from a Document
	 */
	public List<Entry<String, String>> findFields(Document doc) {

        // Get string input options if any
        ArrayList<String> textInputs = new ArrayList <String> ();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("textInputs.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String textInput = null;
        while (true) {
            try {
                textInput = br.readLine();
                if (textInput == null)
                    break;
                else
                    textInputs.add (textInput);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		
		List<Entry<String,String>> mapList = new ArrayList<>();
		
		try {
		
			Elements allTypeInput = doc.getElementsByTag("input");
			Elements allDropDown = doc.getElementsByTag("select"); 		
	        
	        for(Element dropDown:allDropDown){
	        	Elements option = dropDown.getElementsByTag("option");
	        	String optionValues = "";
	        	for (int i=0; i<option.size(); i++){
	        		optionValues += option.get(i).text() + "\n";
	        	}
	        	mapList.add(new SimpleEntry<String,String>(dropDown.attr("name") + "=" + "dropdown", optionValues));
	        }
	
	        for(Element input:allTypeInput){
	            if(input.attr("type").equalsIgnoreCase("checkbox")){
	            	mapList.add(new SimpleEntry<String,String>(input.attr("name") + "=" + "checkbox", "true\nfalse"));
	            }
                else if(input.attr("type").equalsIgnoreCase("text"))
                {
                    String optionValues = "";
                    for (int i = 0; i < textInputs.size(); i++)
                    {
                        optionValues+=textInputs.get(i) + "\n";
                    }

                    mapList.add(new SimpleEntry<String,String>(input.attr("name") + "=" + "text", optionValues));
                }
	           /* else if(input.attr("type").equalsIgnoreCase("button")){
	            	mapList.add(new SimpleEntry<String,String>("button", input.attr("value"))); 
	            }
	            else if(input.attr("type").equalsIgnoreCase("submit")){
	            	mapList.add(new SimpleEntry<String,String>("submit", input.attr("value"))); 
	            }*/
	         }
		}
		
		catch (Exception e)
		{
			e.printStackTrace(System.out);
		}
        
		return mapList;
	}

}

//package JsoupPhase;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.jsoup.nodes.Document;

import com.google.gson.Gson;


public class parseToJson {
	
	public static void main(String[] args){
		jsoupParse test = new jsoupParse();
		formFields form = new formFields();
		parseToJson json = new parseToJson();
		//Document doc = test.parseHTML("http://webappvandv.appspot.com/");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("urls.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String url = null;
        try {
            url = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Document doc = test.parseHTML(url);
		json.parseIntoJson(form.findFields(doc));
	}
	
	public void writeToFile(String jsonInput, String name){
		
		try {
	        String folder = System.getProperty("user.dir");
	        String fileName = folder + "/" + name;
			// TODO: update paths
			FileWriter writer = new FileWriter(fileName);
			writer.write(jsonInput);
			writer.close();	 
		} 
		
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*creates a JSON formatted string using the mapList values
	and writes the output to a JSON file
	*/
	public void parseIntoJson (List<Entry<String, String>> mapList){
		
		Gson gson = new Gson();
		String name, type, values = "";
		String format = "{\"testExplorationMap\":{";
		fieldId id = new fieldId();
		
		try {
		
			for (int i=0; i<mapList.size();i++) {	        	
				String[] length = new String[mapList.size()];
	        	length[i] = mapList.get(i).toString();
	        	String string = length[i];        	
	        	String[] afterSplit = string.split("=");	        	
	        	name = afterSplit[0];
	        	type = afterSplit[1];
		        values = afterSplit[2];	        	
	        	String [] valueSplit = values.split("\n");      	
	        	id.type = type;
	        	id.values = new ArrayList<String>();	        	
	        	for (int k=0; k < valueSplit.length; k++){
	        		id.values.add(valueSplit[k]);
	        	}   			
	        	String tcJson = gson.toJson(id);	    		
	        	if (i == mapList.size()-1) {
	        		format += "\"" + name + "\":" +tcJson + "}}";
	    		}
	    		else {
	    			format += "\"" + name + "\":" +tcJson + ",";
	    		}
 			
	        }
			
			writeToFile(format, "input.txt");
	    	System.out.println(format);
	    	
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
	
	}	
	
	
	public static class fieldId
	{
		public String type;
		public ArrayList<String> values;
	}
	
}

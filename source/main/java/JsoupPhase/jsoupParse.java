//package JsoupPhase;

import java.io.File;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class jsoupParse {
	
	public static void main(String[] args) {
		jsoupParse test = new jsoupParse();
		test.parseHTML("http://webappvandv.appspot.com/");
	}
	
	
	public Document parseHTML (String URL) {
		
		Document document=new Document("");
		
		try
			{
				Connection.Response response = Jsoup.connect(URL)
					.userAgent("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2117.157 Safari/537.36")
					.referrer("http://www.google.com") 
					.timeout(60000)
					.execute();
			
				document = response.parse();
			}
		catch (Exception e)
			{
				e.printStackTrace(System.out);
			}
			return document;
	}
	
	public Document parseFile(String fileName){
		Document document = new Document("");
		try{
			File input = new File(fileName);
			document = Jsoup.parse(input, "UTF-8", "");
		}
		catch (Exception e){
			e.printStackTrace(System.out);
		}
		return document;
	}
	
	
	
}



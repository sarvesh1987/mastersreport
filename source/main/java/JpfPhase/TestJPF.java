import gov.nasa.jpf.vm.Verify;
import com.google.gson.Gson;
import java.util.ArrayList;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;
import static java.nio.file.Files.write;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.io.Serializable;
import java.nio.file.StandardOpenOption;
import java.lang.Exception;
import java.lang.StringBuilder;

public class TestJPF {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Gson gson = new Gson();
		/*
		Test code for writing various maps and reading them back
		TestCase tc = new TestCase();
		tc.type = "checkbox";
		tc.values = new ArrayList<String>();
		tc.values.add("a");
		tc.values.add("b");

		// add a new test case
		TestCase tc1 = new TestCase();
		tc1.type = "dropdown";
		tc1.values = new ArrayList<String>();
		tc1.values.add("foo");
		tc1.values.add("bar");
		tc1.values.add("baz");
		

		// insert into a map
		Map m = new TreeMap<String, TestCase>();
		m.put("field0", tc);
		m.put("field1", tc1);
		TestExplorationMap tem = new TestExplorationMap();
		tem.testExplorationMap = m;
		
		String tcMapJson = gson.toJson(tem, TestExplorationMap.class);
		System.out.println("Testing parsing back");
		TestExplorationMap tem2 = gson.fromJson(tcMapJson, TestExplorationMap.class);
		for (String key: tem2.testExplorationMap.keySet())
		{
			System.out.println(tem2.testExplorationMap.get(key).values);
		}
		
		String tcJson = gson.toJson(tc);
		System.out.println("Printing out map as Json");
		System.out.println(tcMapJson);
		System.out.println(tcJson);
		
		
		*/

		/*
		Read the input data from file and use gson to put it into th eappropriate object
		*/
		String stringFromFile = null;
		
		try{
			stringFromFile = new String(readAllBytes(get("input.txt")));
	
		}
		catch(IOException e){
			System.out.println("IOException occurred");
		}		
		
		TestExplorationMap  mapFromFile = gson.fromJson(stringFromFile, TestExplorationMap.class);

		System.out.println("/////////////////////////////////");
		System.out.println("printing map obtained from file");
		for (String key: mapFromFile.testExplorationMap.keySet())
		{
			TestCase currentTestCase = mapFromFile.testExplorationMap.get(key);
			System.out.println(currentTestCase.type);
			System.out.println(currentTestCase.values);
		}
		System.out.println("/////////////////////////////////");

		/*
		Create an output data structure to put the test cases generated into
		Also create a file to store the data into as we explore the state space
		*/
		OutputTestCaseMap otcMap = new OutputTestCaseMap();
		otcMap.outputTestCaseMap = new TreeMap<String, ArrayList<OutputTestCase>>();
		String tempFile = "temp.txt";
		String otcMapJson = gson.toJson(otcMap, OutputTestCaseMap.class);
		try
		{
			write(get("temp.txt"), otcMapJson.getBytes("utf-8"));
		}
		catch (Exception e)
		{
			System.out.println("An exception occurred");
		}

		/*
		Also create a string representation of the test cases here
		*/
		String testCaseOutputString = "{}";
		try
		{		
			write(get("output.txt"), testCaseOutputString.getBytes("utf-8"));		
		}
		catch (Exception e)
		{
			System.out.println("An exception occured");
		}
		
		testCaseOutputString = "";

		/*
		Use non-deterministic choice to explore the input space
		*/
		int numberOfFields = mapFromFile.testExplorationMap.keySet().size();
		int loopCounter = 0;
		Verify.resetCounter(0);
		for(String key: mapFromFile.testExplorationMap.keySet())
		{
			loopCounter++;	
			TestCase currentTestCase = mapFromFile.testExplorationMap.get(key);
			OutputTestCase otc = new OutputTestCase();
			otc.type = currentTestCase.type;
			if (currentTestCase.type.equals("checkbox"))
			{
				System.out.println("exploring a checkbox");
				String outputJson = null;
				otc.value = Verify.getBoolean()?"true":"false";
				/*
				Read what test cases we already have explored
				*/
				try{
					outputJson  = new String(readAllBytes(get("temp.txt")));
	
				}
				catch(IOException e){
					System.out.println("IOException occurred");
				}
				System.out.println("Printing out the current state");
				System.out.println(outputJson);
				/*
				Use gson to put the current state into a data structure
				*/
				OutputTestCaseMap otcMap1 = gson.fromJson(outputJson, OutputTestCaseMap.class);
				System.out.println("Printing out the number of fields currently present");
				System.out.println(otcMap1.outputTestCaseMap.keySet().size());
				ArrayList<OutputTestCase> arr = otcMap1.outputTestCaseMap.get(key);
				/*
				Add ArrayList to map 
				*/
				if (arr == null)
				{
					System.out.println("ARR IS NULL");
					arr = new ArrayList<OutputTestCase>();
				}
				arr.add(otc);
				otcMap1.outputTestCaseMap.put(key, arr);
				System.out.println("Printing new number of test cases for the current field");
				System.out.println(otcMap1.outputTestCaseMap.get(key).size());
				System.out.println("Printing total number of fields");
				System.out.println(otcMap1.outputTestCaseMap.keySet().size());
				System.out.println("Printing all fields");
				for (String key1 : otcMap1.outputTestCaseMap.keySet())
					System.out.println(key1);
				/*
				Convert new map to Json
				*/
				String jsonString = gson.toJson(otcMap1, OutputTestCaseMap.class);
				System.out.println("Printing updated map json");
				System.out.println(jsonString);

				System.out.println ("printing latest test case added");
				System.out.println(otc.type + " " + otc.value);				
				for (int i = 0 ; i < arr.size(); i++)
				{
					System.out.println(arr.get(i).type + " " + arr.get(i).value);
				}
				/*
				System.out.println("testing output maps");
				OutputTestCase testOtc = new OutputTestCase();
				ArrayList<OutputTestCase> arrOtc = new ArrayList<OutputTestCase>();
				OutputTestCaseMap testMap = new OutputTestCaseMap();
				testMap.outputTestCaseMap = new TreeMap<String, ArrayList<OutputTestCase>>();
				//testMap.outputTestCaseMap.put("field100", arrOtc);
				testMap.outputTestCaseMap.put(key, arr);
				String testString = gson.toJson(testMap, OutputTestCaseMap.class);
				System.out.println(testString);
				*/
				/*
				Write updated map to file
				*/
				try
				{		
					write(get("temp.txt"), jsonString.getBytes("utf-8"));		
				}
				catch (Exception e)
				{
					System.out.println("An exception occured");
				}
				String leadingCharacter = "";
				if (loopCounter != 1)
					leadingCharacter = ", ";
					
				testCaseOutputString = testCaseOutputString + leadingCharacter + "\"" + key + "\"" + ":" + "{\"" + "type" + "\"" + ":" + "\"" + otc.type + "\"" + "," + "\"" + "value" + "\"" + ":" + "\"" + otc.value + "\"" + "}";
				
				/*
					Append to outputTestCaseString
				*/
				StringBuilder str = null;
				if (loopCounter == numberOfFields)
				{
					String testCaseLeadingCharacter = "";
					if (Verify.getCounter(0) != 0)
						testCaseLeadingCharacter = ", ";
					
					testCaseOutputString = testCaseLeadingCharacter + "\"" + "testcase" + Verify.getCounter(0) + "\"" + ":" + "{" + testCaseOutputString  + "}";
					System.out.println(testCaseOutputString);
					try{
						str = new StringBuilder(new String(readAllBytes(get("output.txt"))));
						testCaseOutputString  = (str.insert(str.length()-1, testCaseOutputString)).toString();
	
					}
					catch(IOException e){
						System.out.println("IOException occurred");
					}
				/*
				System.out.println("Printing out last outputString");
				System.out.println(testCaseOutputString);
				String leadingCharacter = "";
				if (loopCounter != 1)
					leadingCharacter = ", ";
					
				testCaseOutputString = testCaseOutputString + leadingCharacter + "\"" + key + "\"" + ":" + "{\"" + "type" + "\"" + ":" + "\"" + otc.type + "\"" + "," + "\"" + "value" + "\"" + ":" + "\"" + otc.value + "\"" + "}";
				*/
					// add trailing bracket to complete test case
					//testCaseOutputString = testCaseOutputString + "}";
					//testCaseOutputString = (str.insert(str.length()-1, testCaseOutputString)).toString();
					Verify.incrementCounter(0);
					System.out.println("Printing output string");
					System.out.println(testCaseOutputString);
					try
					{		
						write(get("output.txt"), testCaseOutputString.getBytes("utf-8"));		
					}
					catch (Exception e)
					{
						System.out.println("An exception occured");
					}
				}   
			}
			else
			{
				if (currentTestCase.type.equals("dropdown") || currentTestCase.type.equals("text"))
				{
					System.out.println("exploring a dropdown box or a text input");
					int index = Verify.getInt(0, currentTestCase.values.size()-1);
					otc.value = currentTestCase.values.get(index);
					String outputJson = null;
				
					try{
						outputJson  = new String(readAllBytes(get("temp.txt")));
	
					}
					catch(IOException e){
						System.out.println("IOException occurred");
					}
					System.out.println("Printing current map state");
					System.out.println(outputJson);
					OutputTestCaseMap otcMap2 = gson.fromJson(outputJson, OutputTestCaseMap.class);
					ArrayList<OutputTestCase> arr = otcMap2.outputTestCaseMap.get(key);
					if (arr == null)
					{
						System.out.println("ARR IS NULL");
						arr = new ArrayList<OutputTestCase>();
					}
					arr.add(otc);
					otcMap2.outputTestCaseMap.put(key, arr);
					System.out.println("Printing current test case");
					System.out.println(otc.type + " " + otc.value);
					String jsonString = gson.toJson(otcMap2, OutputTestCaseMap.class);
					System.out.println("Printin updated map");
					System.out.println(jsonString);
							
					try
					{
						write(get("temp.txt"), jsonString.getBytes("utf-8"));		
					}
					catch (Exception e)
					{
						System.out.println("An exception occured");
					}
					String leadingCharacter = "";
					if (loopCounter != 1)
						leadingCharacter = ", ";
					
					testCaseOutputString = testCaseOutputString + leadingCharacter + "\"" + key + "\"" + ":" + "{\"" + "type" + "\"" + ":" + "\"" + otc.type + "\"" + "," + "\"" + "value" + "\"" + ":" + "\"" + otc.value + "\"" + "}";
					/*
					Append to outputTestCaseString
					*/
					StringBuilder str = null;
					if (loopCounter == numberOfFields)
					{
						String testCaseLeadingCharacter = "";
						if (Verify.getCounter(0) != 0)
							testCaseLeadingCharacter = ", ";
					
						testCaseOutputString = testCaseLeadingCharacter + "\"" + "testcase" + Verify.getCounter(0) + "\"" + ":" + "{" + testCaseOutputString + "}";
						System.out.println("The testCaseLeadingCharacter is" + testCaseLeadingCharacter);
						try{
							str = new StringBuilder(new String(readAllBytes(get("output.txt"))));
							testCaseOutputString  = str.insert(str.length()-1, testCaseOutputString).toString();
	
						}
						catch(IOException e){
							System.out.println("IOException occurred");
						}	
					//System.out.println("Printing out last outputString");
					//System.out.println(testCaseOutputString);
					
					/*
					String leadingCharacter = "";
					if (loopCounter != 1)
						leadingCharacter = ", ";
					
					testCaseOutputString = testCaseOutputString + leadingCharacter + "\"" + key + "\"" + ":" + "{\"" + "type" + "\"" + ":" + "\"" + otc.type + "\"" + "," + "\"" + "value" + "\"" + ":" + "\"" + otc.value + "\"" + "}";
					*/
						// add trailing bracket for test case
						//testCaseOutputString = testCaseOutputString + "}";
						//testCaseOutputString = str.insert(str.length()-1, testCaseOutputString).toString();
						Verify.incrementCounter(0);
						System.out.println("Printing output string");
						System.out.println(testCaseOutputString);
						try
						{		
							write(get("output.txt"), testCaseOutputString.getBytes("utf-8"));		
						}
						catch (Exception e)
						{
							System.out.println("An exception occured");
						}
					}
				}
			}
				
		}

		System.out.println("HELLO BOOYAH");

		//outputMap = Verify.readObjectFromFile(TreeMap.class, tempFile);
		//String outputJson = gson.toJson(outputMap);
		//System.out.println("final output is");
		//System.out.println(outputMap);
		
	}

	public static class TestCase
	{
		public String type;
		public ArrayList<String> values;
	}

	public static class TestExplorationMap
	{
		public Map<String, TestCase> testExplorationMap;
	}

	public static class OutputTestCase
	{
		public String type;
		public String value;
	}
	
	public static class OutputTestCaseMap
	{
		public Map<String, ArrayList<OutputTestCase>> outputTestCaseMap;
	}
}

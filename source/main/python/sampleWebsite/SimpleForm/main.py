#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2

import json

from google.appengine.ext import db

import jinja2

import os

import re

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + '/templates'),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

formOutput = """<form name="myform" action="/" method="POST">
             Pick a type of bread <br>
             <select name="mydropdown">
             <option value="Italian">Italian</option>
             <option value="Wheat">Wheat</option>
             <option value="White">White</option>
             </select><br>
             <input type="checkbox" name="cheese" value="yes">Do you want cheese<br>
             Additional Instructions:<br>
             <input type="text" name="additionalInstructions">
             <br>
             <input type="submit" value="Submit your choice">
             </form>"""

class Order (db.Model):
    bread = db.StringProperty (default=None)
    cheese = db.StringProperty (default=None)
    paymentType = db.StringProperty (default=None)
    orderState = db.StringProperty (default=None)
    additionalInstructions = db.StringProperty (default=None)


class MainHandler(webapp2.RequestHandler):
    def get(self):
        #self.response.write('Hello world!')
        self.response.write(formOutput)

    def post(self):
        bread = self.request.get("mydropdown")
        cheese = self.request.get("cheese")
        additionalInstructions = self.request.get("additionalInstructions")
        #if cheese :
        #    self.response.write(bread + " with cheese")
        #else:
        #    self.response.write(bread + " without cheese")
        newOrder = Order()
        newOrder.bread = bread
        newOrder.cheese = cheese
        newOrder.additionalInstructions = additionalInstructions
        newOrder.orderState = 'Incomplete'
        newOrder.put()
        key = newOrder.key()
        self.redirect('/completeorder?key=' + str(key))

class RepOkHandler (webapp2.RequestHandler):
    def post(self):
        # TODO: Do stuff here
        output = dict()
        q = Order.all()
        allOk = True
        for i in q:
            if i.orderState == 'Complete' and not i.paymentType:
                allOk = False
                break
        if allOk == True:
            output['result'] = 'OK'
        else:
            output['result'] = 'NOT_OK'
        jsonEncoder = json.JSONEncoder()
        outputJson = jsonEncoder.encode(output)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(outputJson)

class CompleteOrderHandler (webapp2.RequestHandler):
    def get(self):
        match = re.search('.*/completeorder\?key=', self.request.uri)
        lenFirstPart = len(match.group(0))
        key = self.request.uri[lenFirstPart:len(self.request.uri)]
        template = JINJA_ENVIRONMENT.get_template('completeorder.html')
        template_values = {'key': key}
        self.response.write(template.render(template_values))

    def post(self):
        paymentType = self.request.get("paymentType")
        key = self.request.get("key")
        order = Order.get(key)
        order.paymentType = paymentType
        order.orderState = 'Complete'
        order.put()
        self.redirect('/')

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/repok.*', RepOkHandler),
    ('/completeorder.*', CompleteOrderHandler)
], debug=True)

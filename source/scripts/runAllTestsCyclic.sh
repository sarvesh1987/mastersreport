#!/bin/bash 
$(cd ../main/resources; cp urls.txt ../java/JsoupPhase)
rm ../main/java/SeleniumPhase/result.txt
for (( i=1; i <= 2; i++ ))
do
	$(cd ../main/java/JsoupPhase; ./runJsoupPhase.sh >/dev/null; cp input.txt ../JpfPhase; cp urls.txt ../SeleniumPhase)
	$(cd ../main/java/JpfPhase; ./runExample.sh >/dev/null; cp output.txt ../SeleniumPhase)
	$(cd ../main/java/SeleniumPhase; ./runJunitTest.sh >>result.txt 2>&1; cp urls.txt ../JsoupPhase)
done
cat ../main/java/SeleniumPhase/result.txt
